# Upgrading process

This upgrade concerns Ubuntu Focal and Bionic for those who want to upgrade to Groovy Gorilla.

**Note: You may encounter some errors during post-install scripts, especially with nvidia-l4t packages, those errors can be skipped as long as the packages version matches.**

```sh
sudo bash -c "echo TEGRA_CHIPID 0x21 | tee /etc/nv_boot_control.conf"

sudo apt-get update
sudo apt-get upgrade

sudo sed -i 's/bionic/groovy/g' /etc/apt/sources.list

sudo bash -c 'echo -e "deb https://repo.download.nvidia.com/jetson/common r32.4 main
deb https://repo.download.nvidia.com/jetson/t210 r32.4 main"
/etc/apt/sources.list.d/nvidia-l4t-apt-source.list'

sudo apt-get update

sudo apt-get -o Dpkg::Options::="--force-overwrite" dist-upgrade
```
