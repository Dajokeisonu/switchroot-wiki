# Features and Programs

The Switch contains a unique combination of hardware allowing for a vast featureset not seen on many other devices. Commonly used programs/apps are listed and documented here for reference.

## Switch Features

JoyCon and Switch Pro Controller native support:

JoyCons may be paired through Linux blutooth settings or through Hekate to allow for use in HOS, Linux, and Android without the need to repair. JoyCons function via Bluetooth, railed on Switch, and connected via USB through the JoyCon charging grip. Multiple pairs can be connected and pressing L/R or SL/SR will choose controller orentation.
Switch Pro Controller functions via Bluetooth and, via USB. Currently there is no easy method (for users) of grabbing the Pro Controller Bluetooth pairing info and it will have to be paired through the Linux/Android Bluetooth settings.

Both JoyCons and Switch Pro Controllers have their IMU (accelerometer/gyro) exposed as udev inputs in Linux and can be used in games/programs with support (Dolphin-Emulator is a good example)

Switch Docked HDMI/Displayport Output Support:

Support for Official Nintendo Switch Dock as well as 3rd party HDMI/Displayport adapters/docks/hubs exists in Linux/Android. Official Dock requires power input from the official power adapter or better. Many 3rd party adapters/docks/hubs can be used without supplemental power or with power from a USB-C PD Power adapter.
Official Nintendo Switch Dock is capable of resolutions up to 4k 30hz or 1440p 60hz. Some 3rd party adapters can also achieve these resolutions. (note, the switch hardware is INCAPABLE of outputing 4k 60hz 4:4:4)

Switch USB-C OTG Support:

OTG via USB-C is enabled and devices can pull up to 1.3A when unpowered.
Powered OTG adapters are not supported, it is advised to use a hub/dock (which usually contains HDMI/Displayport) if USB and powering is desired.

Switch Accelerometer/Gyro and Rotation Support:

Automatic rotation is supported in DE that have it enabled. The built in accelerometer/gyro is NOT exposed in userspace as a udev input.

## Common Applications/Programs

JoyCon Mouse (xserver-xorg-input-joystick):

Many users find it handy to map their JoyCons/Pro Controllers as a mouse/keyboard within Linux.
Installing and loading a custom profile (from the switchroot discord server) can be done with
```sh
cd ~
sudo apt install xserver-xorg-input-joystick -y
sudo rm -rf /usr/share/X11/xorg.conf.d/50-joystick.conf
wget https://cdn.discordapp.com/attachments/527677698672427008/770290009638174750/50-joystick.conf && sudo mv 50-joystick.conf /usr/share/X11/xorg.conf.d
```

Nvidia Power Profile Applet (nvpmodel):

Multiple custom cpu/gpu power profiles are included with the switchroot bionic 3.2.0 and future releases.
Nvidia documetation for nvpmodel GUI can be found at the [link](https://docs.nvidia.com/jetson/l4t/index.html#page/Tegra%2520Linux%2520Driver%2520Package%2520Development%2520Guide%2Fpower_management_nano.html%23wwpID0EPHA)
The nvpmodel config can be found here by default `/etc/nvpmodel/nvpmodel_t210.conf` and more configs can be added by following the examples within.