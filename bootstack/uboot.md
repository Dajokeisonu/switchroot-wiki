# Uboot

[Switch-Uboot](https://gitlab.com/switchroot/bootstack/switch-uboot)

You can build u-boot in two ways, using our docker image or compiling the sources like any other C projects.

## Requirements

GCC 7.4.0 or (Linaro GCC 7.4.0)[https://releases.linaro.org/components/toolchain/binaries/7.4-2019.02/aarch64-linux-gnu/] if you're cross compiling
`sudo apt-get install python python-dev bc curl`

## Configuring uboot scripts location

In `include/config_distro_bootcmd.h` change the `boot_prefixes=` to use the correct path where uboot scripts are located.

## Building

`make nintendo-switch_defconfig`
`make`
