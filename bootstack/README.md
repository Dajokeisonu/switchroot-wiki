# Understanding the Nintendo Switch boot stack 

## Prerequisites

- [Vulnerable Switch](https://gbatemp.net/threads/firmware-status.495078/)
- Micro SDCard (8GB Minimum)

## Micro SD Card Setup

- Format your SD Card, you need at least : \
   &nbsp;Partition 1 for bootfiles and bootloader: \
      &nbsp;&nbsp;type: vfat

   &nbsp;Partition 2 for Gentoo root: \
      &nbsp;&nbsp;type: ext4

- Grab the latest Hekate release, [here](https://github.com/CTCaer/hekate/releases/) and extract it to `vfat:/`
- Note: Hekate is able to resize vfat partition and create new partitions for Linux. The creating of ext4 file system is done during installation

## Bootchain explanation

1. **Hekate** uses the config files in `vfat:/bootloader/ini/*.ini` for boot entries. If a boot entry is selected, Hekate then executes the payload set in this `.ini` file, an example here with *Gentoo*: `payload=switchroot/gentoo/coreboot.rom`

2. **Coreboot** is the **BIOS**. Inside of the Coreboot the **U-boot** bootloader is built in.

3. **U-boot** loads the configuration file `vfat:switchroot/gentoo/boot.scr`. Every distro provides his own coreboot.rom with modified u-boot pointing to the correct **switchroot/*distro*** folder.

4. The **boot.scr** or *U-boot script*, is responsible of booting the linux kernel. The process is the following :
   1. load linux kernel `Image` into memory,
   2. load `tegra210-icosa.dtb` into memory,
   3. load `initramfs` (if provided) into memory,
   4. load then read the parameters set in uenv.txt file
   5. load configured overlays
   6. boot the linux kernel in the prepared environment

5. If an **initramfs** is provided, the Kernel uses it as *initial root*. The initramfs does the next magic:
   1. Resize the root partition if necessary on first boot
   2. Extract/Install the kernel modules, `modules.tar.gz`, into `root:/lib/modules` if provided (external kernel upgrade)
   3. Install `update.tar.gz` into `vfat:switchroot/distro` if provided
   4. Generates bluetooth pairing cache based on provided `vfat:switchroot/joycon_mac.ini`
   5. Do some updates on root
   5. Initialize the framebuffer
   6. Change the root into prepared root partition

*Note: If no initramfs is provided, the kernel runs directly on root partition without preparations*

6. The future processing is depending on distro and used init system. Linux boots!
